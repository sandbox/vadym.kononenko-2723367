<?php

/**
 * @author      Vadym Kononenko
 *
 * Drupal module file for common hooks.
 */

//define('USERPOINTS_FIELDS_CONFIGURATION_URI', 'admin/config/people/userpoints/operations');
define('USERPOINTS_FIELDS_CONFIGURATION_URI', 'admin/structure/userpoints');

/**
 * Implements hook_menu().
 */
function userpoints_fields_ui_menu() {
  $items = array();
  $items[USERPOINTS_FIELDS_CONFIGURATION_URI] = array(
    'title' => '!Points',
    'title arguments' => userpoints_translation(),
    'description' => strtr('Configuration for !points operations.', userpoints_translation()),
    'type' => MENU_NORMAL_ITEM,
    'file path' => drupal_get_path('module', 'system'),
    'access arguments' => array('administer userpoints'),
  );

  $entity_info = entity_get_info('userpoints_transaction');

  foreach ($entity_info['bundles'] as $type => &$bundle) {
    // Prepare 'name' from machine name.
    $name = userpoints_fields_type_to_name($type);

    // Prepare menu item arg from userpoint type.
    $type_arg = userpoints_fields_type_to_arg($type);

    $items[USERPOINTS_FIELDS_CONFIGURATION_URI . '/manage/' . $type_arg] = array(
      'title' => '@name !points operation',
      'title arguments' => userpoints_translation() + array(
        '@name' => $name,
      ),
      'description' => strtr('Configuration for @name !points operations.', userpoints_translation() + array(
        '@name' => $name,
      )),
      'type' => MENU_NORMAL_ITEM,
      'page callback' => 'userpoints_fields_ui_tabs_overview',
      'access arguments' => array('administer userpoints'),
    );
    $items[USERPOINTS_FIELDS_CONFIGURATION_URI . '/manage/' . $type_arg . '/overview'] = array(
      'title' => 'Overview',
      'description' => 'Overview page.',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
  }

  return $items;
}

/**
 * Callback returns the userpoints settings pages overview.
 *
 * @return rendered html
 */
function userpoints_fields_ui_tabs_overview() {
  $local_tasks = menu_local_tasks();

  $links = array(
    '#theme' => 'links',
    '#links' => array(),
  );

  foreach($local_tasks['tabs']['output'] as $tab) {
    $links['#links'][] = array(
      'title' => $tab['#link']['title'],
      'href' => $tab['#link']['path'],
    );
  }

  return drupal_render($links);
}

/**
 * Implements hook_entity_info_alter().
 */
function userpoints_fields_ui_entity_info_alter(&$entity_info) {
  // Use userpoints transaction bundles. Field API requires it.
  foreach ($entity_info['userpoints_transaction']['bundles'] as $type => &$bundle) {
    $type_arg = userpoints_fields_type_to_arg($type);

    $bundle['admin'] = array(
      // Extend existing bundles properties by 'path' options.
      'path' => USERPOINTS_FIELDS_CONFIGURATION_URI . '/manage/' . $type_arg,
      'access arguments' => array('administer userpoints'),
    );
  }
}
